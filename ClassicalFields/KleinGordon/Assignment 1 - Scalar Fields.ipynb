{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import init  # Sets some MathJax for equations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": "true"
   },
   "source": [
    "<h1>Table of Contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Assignment-1:-Scalar-Fields\" data-toc-modified-id=\"Assignment-1:-Scalar-Fields-1\"><span class=\"toc-item-num\">1&nbsp;&nbsp;</span>Assignment 1: Scalar Fields</a></span><ul class=\"toc-item\"><li><span><a href=\"#Classical-Mechanics\" data-toc-modified-id=\"Classical-Mechanics-1.1\"><span class=\"toc-item-num\">1.1&nbsp;&nbsp;</span>Classical Mechanics</a></span></li><li><span><a href=\"#Green's-Functions\" data-toc-modified-id=\"Green's-Functions-1.2\"><span class=\"toc-item-num\">1.2&nbsp;&nbsp;</span>Green's Functions</a></span></li><li><span><a href=\"#Numerical-Simulations\" data-toc-modified-id=\"Numerical-Simulations-1.3\"><span class=\"toc-item-num\">1.3&nbsp;&nbsp;</span>Numerical Simulations</a></span></li><li><span><a href=\"#Travelling-Waves\" data-toc-modified-id=\"Travelling-Waves-1.4\"><span class=\"toc-item-num\">1.4&nbsp;&nbsp;</span>Travelling Waves</a></span></li><li><span><a href=\"#Wave-Packets\" data-toc-modified-id=\"Wave-Packets-1.5\"><span class=\"toc-item-num\">1.5&nbsp;&nbsp;</span>Wave Packets</a></span></li><li><span><a href=\"#Green's-Function\" data-toc-modified-id=\"Green's-Function-1.6\"><span class=\"toc-item-num\">1.6&nbsp;&nbsp;</span>Green's Function</a></span></li><li><span><a href=\"#Solitons\" data-toc-modified-id=\"Solitons-1.7\"><span class=\"toc-item-num\">1.7&nbsp;&nbsp;</span>Solitons</a></span></li></ul></li><li><span><a href=\"#Complex-Scalar-Field\" data-toc-modified-id=\"Complex-Scalar-Field-2\"><span class=\"toc-item-num\">2&nbsp;&nbsp;</span>Complex Scalar Field</a></span><ul class=\"toc-item\"><li><span><a href=\"#Kibble-Zurek-Mechanism-(Incomplete)\" data-toc-modified-id=\"Kibble-Zurek-Mechanism-(Incomplete)-2.1\"><span class=\"toc-item-num\">2.1&nbsp;&nbsp;</span>Kibble-Zurek Mechanism (Incomplete)</a></span></li></ul></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Assignment 1: Scalar Fields"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the equations for classical scalar field theory.  The dynamics are described by the Klein-Gordon equation:\n",
    "\n",
    "\\begin{gather}\n",
    "  \\partial_\\mu\\partial^\\mu\\phi = \n",
    "  -\\partial_t^2\\phi + \\nabla^2\\phi = \\pdiff{V(\\phi)}{\\phi} = 0,\n",
    "  \\tag{real}\\\\ \n",
    "  \\partial_\\mu\\partial^\\mu\\Phi = \n",
    "  -\\partial_t^2\\Phi + \\nabla^2\\Phi = \\pdiff{V(\\Phi, \\Phi^*)}{\\Phi^*} = 0.\n",
    "  \\tag{complex}\n",
    "\\end{gather}\n",
    "\n",
    "Here we follow 't Hooft using a metric with signature $g = \\diag(-1,1,1,1)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Classical Mechanics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Show that the Klein-Gordon equation follows from the principle of least action with the following Lagrangian densities:\n",
    "\n",
    "\\begin{gather}\n",
    "  \\mathcal{L}[\\phi] = \\frac{1}{2}\\partial_\\mu\\phi\\partial^\\mu\\phi - V(\\phi), \\tag{real}\\\\\n",
    "  \\mathcal{L}[\\Phi, \\Phi^*] = \\partial_\\mu\\Phi^*\\partial^\\mu\\Phi - V(\\Phi, \\Phi^*).\n",
    "  \\tag{complex}\n",
    "\\end{gather}\n",
    "\n",
    "  By Lagrangian density, we mean that the action for such a system results from integrating over space and time:\n",
    "  \n",
    "  $$\n",
    "    S = \\int \\d{t}\\;\\d^{D}{x}\\; \\mathcal{L}\n",
    "  $$\n",
    "\n",
    "* Identify the canonical momenta, and compute the Hamiltonian in each case.\n",
    "* Prove that, if the potential $V$ is independent of time, that the Hamiltonian (the total energy of the system) is conserved.\n",
    "* Prove that if $V(\\Phi, \\Phi^*) = V(e^{\\I\\theta}\\Phi, e^{-\\I\\theta}\\Phi^*)$ is invariant under phase rotations, that there is a conserved charge that is conserved.  Note: this charge is not particle number, but in some sense is the sum of particles minus antiparticles.\n",
    "* Describe a physical system that would be describe by the Klein-Gordon equation.  *(Hint: think balls and springs.  What does $\\phi$ represent?  How would you implement a potential $V(\\phi)$?  Keep this model in mind as you interpret your later results.)*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Green's Functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the following definition of a Fourier transform for the Green's function:\n",
    "\n",
    "$$\n",
    "  G(x) = G(t, \\vect{x}) \n",
    "       = \\int \\frac{\\d^{1+D} k}{(2\\pi)^{1+D}} e^{\\I k_\\mu x^\\mu}G_{k}\n",
    "       = \\int \\frac{\\d k_0}{(2\\pi)} e^{-\\I k_0 t}\n",
    "         \\int \\frac{\\d^{D} \\vect{k}}{(2\\pi)^{D}} e^{\\I \\vect{k}\\cdot\\vect{x}}\n",
    "         G_{k_0, \\vect{k}}\n",
    "$$\n",
    "\n",
    "where $G_k = G_{k_0, \\vect{k}}$ is the momentum/frequency space representation of the Green's function. Consider the Green's functions for a massive real scalar theory with no non-linear interactions: i.e. $V(\\phi) = m^2\\phi^2/2$:\n",
    "\n",
    "$$\n",
    "  (m^2 - \\partial_\\mu\\partial^\\mu)G(x - y) = \\delta^{1+D}(x-y).\n",
    "$$\n",
    "\n",
    "* Compute the advance $G^{+}$, retarded $G^{-}$, and Feynman $G^{F}$ Green's functions in frequency/momentum space $G_{k_0, \\vect{k}}$, arguing that the boundary conditions can be implemented appropriately by shifting the poles of the Green's function by an appropriate insertion imaginary infinitesimal $\\I 0^+$.\n",
    "* Show explicitly that these choices are correct by computing the time/momentum space forms $G_{\\vect{k}}(t)$ explicitly using contour integration to evaluate the integral over $k_0$.\n",
    "* (Bonus): Compute the time/position space forms in 1+1 dimensions ($t$ and $x$) by explicitly integrating over $k_x$.  (In particular, compute the retarded Green's function which we will explore numerically later.) *The expression for $G_F$ is given in [Z. Hong-Hao et al. (2010) Chinese Phys. C 34 1576](https://doi.org/10.1088/1674-1137/34/10/005) ([arXiv:0811.1261](https://arxiv.org/abs/0811.1261))*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Numerical Simulations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Write a code to numerically solve the Klein-Gordon equation in a periodic box of length $L$ given an initial solution at time $t=0$ specified by $\\phi(t=0, x)$ and $\\dot{\\phi}(t=0, x)$.  In the class CoCalc project you will find a solution to this problem in python using a spectral methods for a periodic mesh where the spatial derivatives are computed to high accuracy using the [FFT](https://en.wikipedia.org/wiki/Fast_Fourier_transform).  You may use a finite-difference approach if you prefer, but make sure your solutions are sufficiently accurate.\n",
    "\n",
    "Use this numerical code to check the following behaviour:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Travelling Waves"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the homogeneous Klein-Gordon equation with mass $m^2 = a$:\n",
    "\n",
    "$$\n",
    "  V(\\phi) = \\frac{a}{2}\\phi^2.\n",
    "$$\n",
    "\n",
    "* Argue that any separable solution of the form $f(x)g(t)$ must have:\n",
    "\n",
    "  $$\n",
    "    f''(x) = - k^2 f(x), \\qquad\n",
    "    \\ddot{g}(t) = - \\omega^2 g(t)\n",
    "  $$\n",
    "  \n",
    "  with $\\omega^2 = k^2 + m^2$.\n",
    "    \n",
    "* Show that this implies that the following traveling wave is a solution:\n",
    "\n",
    "  $$\n",
    "    \\phi(t, x) = A\\cos(kx - \\omega t).\n",
    "  $$\n",
    "  \n",
    "  Demonstrate this solution numerically using your code.\n",
    "\n",
    "The motion of these wave-like solutions is given by the \"dispersion\" relationship:\n",
    "\n",
    "$$\n",
    "  \\omega(k) = E(k) = \\sqrt{k^2 + m^2}.\n",
    "$$\n",
    "\n",
    "* Describe what happens to these solutions if you now add a non-linearity $\\lambda > 1$ to your equations:\n",
    "\n",
    "  $$\n",
    "    V(\\phi) = \\frac{m^2}{2}\\phi^2 + \\frac{\\lambda}{4}\\phi^4.\n",
    "  $$\n",
    "  \n",
    "  *Note that the normalization of the $\\phi^4$ term here differs from that in 't Hooft's notes.  The convention here makes some of the analytic solutions presented simpler, but is less useful for perturbative calculations.*\n",
    "  \n",
    "* (Bonus) There is an exact solution for these traveling wave solutions in the presence of a non-linearity $\\lambda > 0$ *[M. Frasca (2011) J.Nonlin.Math.Phys.18:291-297](http://dx.doi.org/10.1142/S1402925111001441) ([arXiv:0907.4053](https://arxiv.org/abs/0907.4053))*:\n",
    "\n",
    "  $$\n",
    "    \\DeclareMathOperator{\\sn}{sn}\n",
    "    \\phi(x, t) = \\pm A\\sn\\left(\n",
    "       kx - \\omega t + \\theta;\n",
    "       \\frac{m^2 - \\sqrt{m^4+2\\lambda\\mu^4}}\n",
    "            {m^2 + \\sqrt{m^4+2\\lambda\\mu^4}}\n",
    "    \\right),\\qquad\n",
    "    A = \\sqrt{\\frac{2\\mu^4}{m^2 + \\sqrt{m^4+2\\lambda\\mu^4}}},\n",
    "  $$\n",
    "  \n",
    "  where $\\sn(u, \\tilde{m})$ is a [Jacobi elliptic function](https://en.wikipedia.org/wiki/Jacobi_elliptic_functions) which generalize the sine and cosine functions.  *(In the paper, they use the form $\\sn(u; \\tilde{k}) = \\sn(u, \\tilde{m})$ where $\\tilde{m} = \\tilde{k}^2$.  Here I use $\\tilde{k}$ and $\\tilde{m}$ to correspond with the usual arguments $\\sn(u, m)$ or $\\sn(u, k)$ which these have nothing to do with the physical parameters.  They also use slightly different notation $m \\rightarrow \\mu_0$, $k \\rightarrow p$, and introduce integration constants $\\mu$ and $\\theta$.)*\n",
    "  \n",
    "  Note that the period of $\\sn(u, \\tilde{m})$ is given in terms of the complete elliptic integral of the first kind $K$:\n",
    "  \n",
    "  $$\n",
    "    4K(\\tilde{m}) = 4 \\int_{0}^{\\pi/2}\\frac{1}{\\sqrt{1-\\tilde{m}\\sin^2\\theta}}\\d{\\theta}\n",
    "  $$\n",
    "  \n",
    "  Demonstrate this solution numerically.  \n",
    "  \n",
    "  An interesting feature of this solution is that they behave as if they have an effective mass $m_*$ instead of the bare mass $m$:\n",
    "\n",
    "  $$\n",
    "    \\omega^2(k) = k^2 + m_*^2, \\qquad\n",
    "    m_*^2 = m^2 + \\frac{\\lambda A^2}{2}\n",
    "  $$\n",
    "  \n",
    "  where the $A$ is the amplitude of the wave.  This effective mass persists even if the bare mass $m=0$ is zero.  Note that in the limit of small amplitude, $A\\rightarrow 0$ and $\\mu \\rightarrow 0$, so that the effects of the non-linearity which enter as $\\lambda \\mu^4$ and $\\lambda A^2$ becomes insignificant.  Hence the previous $\\lambda = 0$ results are always valid for small enough amplitude vibrations.\n",
    "\n",
    "  An alternative formulation of the problem is as follows (replacing $\\mu$ for the amplitude $A$):\n",
    "  \n",
    "  $$\n",
    "    \\DeclareMathOperator{\\sn}{sn}\n",
    "    \\phi(x, t) = \\pm A\\sn\\left(\n",
    "       kx - \\omega t + \\theta;\n",
    "       \\frac{m^2 - \\omega^2 + k^2}\n",
    "            {\\omega^2 - k^2}\n",
    "    \\right),\\qquad\n",
    "    A = \\sqrt{2\\frac{\\omega^2 - m^2 - k^2}{\\lambda}}\n",
    "  $$\n",
    "\n",
    "  If you did not have this formula (or could not find an appropriate implementation of $\\sn(u, \\tilde{m})$), how could you numerically produce the initial state?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Wave Packets"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From this dispersion one can find the phase and group velocities for various waves and wavepackets:\n",
    "\n",
    "$$\n",
    "  v_{\\mathrm{phase}} = \\frac{\\omega(k)}{k} \\geq 1, \\qquad\n",
    "  v_{\\mathrm{group}} = \\omega'(k) = \\frac{k}{\\omega(k)} \\leq 1.\n",
    "$$\n",
    "\n",
    "* Show that a wave-packet of the following form provides an approximate solution when the envelope $f$ is sufficiently large:\n",
    "\n",
    "  $$\n",
    "    \\phi(t, x) \\approx f(x-v_{\\mathrm{group}} t)\n",
    "                       \\cos\\bigl(p(x - v_{\\mathrm{phase}} t)\\bigr).\n",
    "  $$\n",
    "  \n",
    "  Demonstrate numerically the behavior of such wavepackets.  In particular, show that for small $p$ they move slower than the speed of light $c=1$ and for large $p$ they approach $c$.\n",
    "  \n",
    "* Add back a non-linearity to your equations $\\lambda > 1$.  Discuss how such wavepackets now behave.\n",
    "\n",
    "* (Bonus) Find a good solution (analytic, approximate, or numeric) for slowly moving (or stationary) small wavepackets.  *(I have not been able to find an exact solution for these, but I suspect that they exist.  This might be a hard problem.)*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Green's Function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Numerically simulate the retarded Green's Function $G^{-}(t, x)$ for the massive Klein Gordon equation (without any interactions) by starting with the vacuum state $\\phi = \\dot{\\phi} = 0$ and hitting it with a \"delta-function\" source:\n",
    "\n",
    "  $$\n",
    "    \\ddot{\\phi} = (\\nabla^2 - m^2)\\phi + J(t, x), \\qquad\n",
    "    J(t, x) = \\lim_{\\sigma\\rightarrow 0} \\frac{1}{2\\pi \\sigma}e^{-(t^2 + x^2)/2\\sigma^2}.\n",
    "  $$\n",
    "\n",
    "  *(I found that choosing $\\sigma \\approx 2\\d{x}$ where $\\d{x}$ is the lattice spacing works quite well, but check your convergence to make sure.)*\n",
    "* (Bonus) Compare your answer to the analytic solution you derived above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solitons"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now consider the case where $a = m^2 = -\\lambda F^2 < 0$. \n",
    "\n",
    "$$\n",
    "  V(\\phi) = \\frac{\\lambda}{4}(\\phi^2 - F^2)^2 - \\frac{\\lambda F^2}{4}.\n",
    "$$\n",
    "\n",
    "* Show that the following soliton is a solution:\n",
    "\n",
    "  $$\n",
    "    \\phi(t, x) = F\\tanh(px - \\omega t), \\qquad\n",
    "    \\omega^2 = p^2 - \\frac{\\lambda F^2}{2}.\n",
    "  $$\n",
    "  \n",
    "* Demonstrate this solution numerically.  What happens if two solitons collide? Do they scatter?  How does the scattering depend on their energy?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Complex Scalar Field"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider now a complex scalar field\n",
    "\n",
    "$$\n",
    "  \\Phi = \\frac{\\phi_1 + \\I\\phi_2}{\\sqrt{2}}, \\qquad\n",
    "  \\mathcal{L}[\\Phi, \\Phi^*] = \\partial_\\mu\\Phi^*\\partial^\\mu\\Phi - V(\\Phi, \\Phi^*), \\qquad\n",
    "  \\ddot{\\Phi} = \\nabla^2\\Phi - \\pdiff{V(\\Phi, \\Phi^*)}{\\Phi^*}.\n",
    "$$\n",
    "\n",
    "Assume the potential is such that the action is invariant under the following global phase transformation:\n",
    "\n",
    "$$\n",
    "  \\Phi \\rightarrow e^{\\I\\theta}\\Phi, \\qquad\n",
    "  \\Phi^* \\rightarrow e^{-\\I\\theta}\\Phi^*.\n",
    "$$\n",
    "\n",
    "* Use Noether's theorem to derive the corresponding conserved current $J^{\\mu}$ and identify the conserved charge $Q = \\int\\d^D{\\vect{x}} J^0$.\n",
    "* Note that the equations of motion for the complex field reduce to those for a real scalar field if $\\Phi = \\Phi^*$: i.e. if $\\phi_1 = \\phi$, and $\\phi_2 = 0$.  Explain how the complex theory can have a conserved charge even though the real scalar theory does not.  In other words, what do purely real fields represent in the complex theory in terms of $Q$?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Kibble-Zurek Mechanism (Incomplete)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Note: This problem is incomplete... the essence is included in the sample code, but I need to test it a bit further before assigning an exploration of this mechanism.  Feel free to play with this if you are interested and let me know what you discover.  Can you demonstrate the Kibble-Zurek scaling laws?*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the behavior of a scalar field with $a < 0$ in the early universe.  Initially the field will be in some random configuration.  As the universe expands, the temperature decreases and the field relaxes into the ground states $\\phi = \\pm F$, however, due to the initial random distribution, the universe will be separated into domains with differing sign, separated by the solitons discussed above.\n",
    "\n",
    "The final number/size of the domains depends on how quickly one passes through the phase transition and is characterized by scaling relations derived by [Kibble and Zurek](https://en.wikipedia.org/wiki/Kibble%E2%80%93Zurek_mechanism).\n",
    "\n",
    "To model this, we add a time-dependent scale factor to the metric: $g = \\diag[-1, a^{-2}(t)]$ where the scale factor $a(t)$ changes with [Hubble constant](https://en.wikipedia.org/wiki/Hubble%27s_law) $H = \\dot{a}(t)/a(t)$.\n",
    "\n",
    "The resulting equations of motion become:\n",
    "\n",
    "$$\n",
    "  \\mathcal{L} = \\frac{1}{2}\\left(-\\dot{\\phi}^2 + DH\\dot{\\phi}- \\frac{1}{a^{2}(t)}(\\nabla\\phi)^2\\right) - V(\\phi),\\\\\n",
    "  \\ddot{\\phi} = \\frac{1}{a^{2}(t)}\\nabla^2\\phi -DH\\dot{\\phi} - V'(\\phi).\n",
    "$$"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python [conda env:work]",
   "language": "python",
   "name": "conda-env-work-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {
    "height": "156px",
    "width": "252px"
   },
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {},
   "toc_section_display": "block",
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
